const User = require('../models/Users');
const bcrypt = require('bcrypt');

// Check if email aready exists
// 1. Use mongoose "find" method to find duplicate emails
// 2. Use the then "method" to send a response to the frontend app based on the result of the find method 
const checkEmailExists = (request, response) =>{
	
	// The result is sent back to the front end via the "then" method
	return User.find({email: request.body.email}).then(result => {
		let message;
		// The "find" method returns an array of record of matching documents

		if(result.length > 0) {
			message = `The ${request.body.email} is already taken. Please use another email.`
			return response.send(message);
		} else {
			message = `The email ${request.body.email} is available.`
			return response.send(message);
		}
	})
}

const registerUser = (request, response) => {

	// creates a variable named "newUser" and instantiates a new 'User' object using mongoose model
	// Uses the information from request body to provide the necessary information
	let newUser = new User({
		firstName: request.body.firstName,
		lastName: request.body.lastName,
		email: request.body.email,

		// salt - salt rounds that bcrypt algorithm will run to encrypt the password
		password: bcrypt.hashSync(request.body.password, 10),
		mobileNo: request.body.mobileNo
	})

	// Saves the created object to our database
	return newUser
	.save()
	.then(user => {
		console.log(user);
		response.send(`Thank you, ${user.firstName}. You are now registered.`);
	}).catch(error => {
		console.log(error);
		response.send(`Sorry, ${newUser.firstName}, there was an error during the registration. Please try again.`)
	})
}

// User Authentication
// 1. Check db if the user email exists
// 2. Compare the password provided in the login form with the password stored in the db
const loginUser = (request, response) => {
	return User.findOne({email: request.body.email})
	.then(result => {
		console.log(result);
		if(result === null) {
			response.send(`This email doesn't exist.`)
		} else {
			// compareSync() method is used to compare a non encrypted password from the login form to the encrypted password retrieved
			// it will return true or false value depending on the result
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

			if(isPasswordCorrect){
				return response.send(`Logged in successfully!`);
			} else {
				return response.send(`Incorrect password, please try again.`);
			}
		}
	})
}

const getProfile = (request, response) => {
	return User.findOne({_id: request.body.id})
	.then(result => {
		if (result === null) {
			return response.send(`ObjectId is not found!`)
		} else {
			result.password = "*********";
			return response.send(result);
		}
	})
}

module.exports = {
	checkEmailExists,
	registerUser,
	loginUser,
	getProfile
}