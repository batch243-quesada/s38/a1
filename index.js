require('dotenv').config()

// dependencies
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const userRoutes = require('./routes/userRoutes');

// express app
const app = express();

// middlewares
app.use(cors());
app.use(express());
app.use(express.json()); // for parsing
app.use(express.urlencoded({extended: true}));

// routes
app.use("/users", userRoutes)

// db connection
mongoose.connect(process.env.MONGO_URI, 
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	)

// .on to catch future connection errors
mongoose.connection.on("error", console.error.bind(console, "connection error"));
mongoose.connection.once('open', () => console.log("Now connected to MongoDB Atlas."));

app.listen(process.env.PORT, () => {
	console.log(`API is now online on port ${process.env.PORT}`);
});