const express = require('express');
const router = express.Router();

const userController = require('../controllers/userControllers');

// routes
router.post('/checkEmail', userController.checkEmailExists);

router.post('/register', userController.registerUser);

router.post('/login', userController.loginUser);

router.post('/details', userController.getProfile);

// router.post('/details', (request, response) => {
// 	userController.getProfile(request.params.id, request.body)
// 	.then(result => response.send(result));
// });

module.exports = router;